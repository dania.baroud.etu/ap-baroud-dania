from date import Date
from etudiant import Etudiant
def pour_tous(seq_bool: list[bool]) -> bool:
    """
    Renvoie True ssi `seq_bool` ne contient pas False

    Exemples:

    $$$ pour_tous([])
    True
    $$$ pour_tous((True, True, True))
    True
    $$$ pour_tous((True, False, True))
    False
    """
    res = True
    resul= False
    for i in seq_bool:
         if not i:
             return resul
    return res 


def il_existe(seq_bool: list[bool]) -> bool:
    """

    Renvoie True si seq_bool contient au moins une valeur True, False sinon

    Exemples:

    $$$ il_existe([])
    False
    $$$ il_existe((False, True, False))
    True
    $$$ il_existe((False, False))
    False
    """
    res = True
    resul= False
    for i in seq_bool:
        if i :
           return res
    return resul

def charge_fichier_etudiants(fname: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants présents dans le fichier dont
    le nom est donné en paramètre.

    précondition: le fichier est du bon format.
    """
    res = []
    with open(fname, 'r') as fin:
        fin.readline()
        ligne = fin.readline()
        while ligne != '':
            nip, nom, prenom, naissance, formation, groupe = ligne.strip().split(';')
            y, m, d = naissance.split('-')
            date_naiss = Date(int(d.lstrip('0')), int(m.lstrip('0')), int(y))
            res.append(Etudiant(nip, nom, prenom, date_naiss, formation, groupe))
            ligne = fin.readline()
    return res

L_ETUDIANTS = charge_fichier_etudiants('etudiants.csv')       
        
COURTE_LISTE =L_ETUDIANTS [:10]

def est_liste_d_etudiants(x) -> bool:
    """
    Renvoie True si ``x`` est une liste de fiches d'étudiant, False dans le cas contraire.

    Précondition: aucune

    Exemples:

    $$$ est_liste_d_etudiants(COURTE_LISTE)
    False
    $$$ est_liste_d_etudiants("Timoleon")
    False
    $$$ est_liste_d_etudiants([('12345678', 'Calbuth', 'Raymond', 'Danse', '12') ])
    False
    """
    return isinstance(x, list) and all(isinstance(etudiant, tuple) and len(etudiant) == 3 for etudiant in x)


NBRE_ETUDIANTS = 604

NIP = 42318171

indice_etudiant= NIP % len(L_ETUDIANTS)
etudiant_trouve = L_ETUDIANTS[indice_etudiant]

# question5-> 49188132 : Christelle DIAZ


#date_reference = 2004, 2, 2
#etudiants_moins_20_ans = [etudiant.naissance for etudiant in L_ETUDIANTS if etudiant.naissance > date_reference]

from typing import List, Set, Tuple 

def ensemble_des_formations(liste: list[Etudiant]) -> Set[str]:
    """
    Renvoie un ensemble de chaînes de caractères donnant les formations
    présentes dans les fiches d'étudiants

    Précondition: liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ ensemble_des_formations(COURTE_LISTE)
    {'MI', 'PEIP', 'MIASHS', 'LICAM'}
    $$$ ensemble_des_formations(COURTE_LISTE[0:2])
    {'MI', 'MIASHS'}
    """
    formations = set()
    for etudiant in liste:
        formations.add(etudiant.formation)
    return formations
          
  
def compter_prenoms(liste:list):
    """fonction qui compte le nombre d'occurences de chaque prenom dans la liste d'etudiant

    Précondition : 
    Exemple(s) :
    $$$ compter_prenoms(COURTE_LISTE) 
    {'Eugène':1,'Josette':1,'Benoît':1,'David':2,'Andrée':1,'Cécile':1,'Christiane':1,'Paul':1,'Anne':1}
    """
    res = {}
    for etud in liste:
        prenom = etud.prenom
        if prenom in res:
            res[prenom] = res[prenom] + 1
        else:
            res[prenom] = 1
    return res
#il y a 2 Camille et 3 Alexandre
#elle necessite 1 parcours

#question 9: il y a 198 prénoms différents parmi tous les etudiants
#question10: le prenom le plus fréquent est Margot

def occurences_nip(liste):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ occurences_nip(COURTE_LISTE)
    
    """
    res = True
    resul = False
    nips = set()
    for etudiant in liste:
        nip =etudiant.get('nip')
        if nip in nips:
            return resul
        nips.add(nip)
    return res

def compter_formations(liste):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    formations={}
    for etudiant in liste:
        formation = etudiant.get('formation')
        if formation in formations:
            formations[formation] += 1
        else:
            formations[formation] = 1
    return formations

def liste_formation(liste: list[Etudiant], form: str) -> list[Etudiant]:
    """
    Renvoie la liste des étudiants de la formation ``form``

    Précondition:  liste ne contient que des fiches d'étudiants

    Exemples:

    $$$ l_MI = liste_formation(COURTE_LISTE, 'MI')
    $$$ len(l_MI)
    6
    $$$ type(l_MI[1]) == Etudiant
    True
    $$$ len(liste_formation(L_ETUDIANTS, 'INFO'))
    0
    """
    return [etudiant for etudiant in liste if etudiant.get('formation') == form]

                