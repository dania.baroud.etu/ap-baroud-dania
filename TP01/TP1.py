# 1)s.split(' '): ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#   s.split('e'): ['la méthod', ' split ', 'st parfois bi', 'n util', '']
#   s.split('é'): ['la m', 'thode split est parfois bien utile']
#   s.split()   : ['la', 'méthode', 'split', 'est', 'parfois', 'bien', 'utile']
#   s.split('') : ValueError
#   s.split('split'):['la méthode ', ' est parfois bien utile']
# 2) la méthode split divise une chaine de caractere en une liste de sous chaine. Elle permet ainsi de séparer une chaine en plusieurs partie
# 3) non, elle retourne une nouvelle liste de sous chaine. La chaine originale ne change pas

# 1)"".join(l) : 'laméthodesplitestparfoisbienutile'
#   " ".join(l): 'la méthode split est parfois bien utile'
#   ";".join(l): 'la;méthode;split;est;parfois;bien;utile'
#   " tralala ".join(l): 'la tralala méthode tralala split tralala est tralala parfois tralala bien tralala utile'
#   print ("\n".join(l)): la
#                         méthode
#                         split
#                         est
#                         parfois
#                         bien
#                         utile
#   "".join(s) : ' la méthode split est parfois bien utile'
#   "!".join(s): ' !l!a! !m!é!t!h!o!d!e! !s!p!l!i!t! !e!s!t! !p!a!r!f!o!i!s! !b!i!e!n! !u!t!i!l!e'
#   "".join()  : TypeError
#   "".join([]): ''
#   "".join([1,2]): TypeError
# 2) la méthode join est utiliser pour concatener les éléments d'une séquence en une seule chaine de caractère
# 3) la methode join ne modifie pas la liste d'origine , elle retourne simplement une nouvelle chaine 
# 4)
def join(c:str, l:list[str])->str:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ join('.', ['raymond', 'calbuth', 'ronchin', 'fr'])
    'raymond.calbuth.ronchin.fr'
    """
    if not l:
        return ""
    res = l[0]
    for i in l[1:]:
        res += c + i
    return res

def sort(s:str)->str:
    """Renvoie une chaine de caractères contenant les caractères de 's' triés

    Précondition : 
    Exemple(s) :
    $$$ sort('timoleon')
    'eilmnoot'

    """
    c_list= list(s)
    c_list.sort()
    res = ''.join(c_list)
    return res

def sont_anagrammes(s1:str, s2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes('Orange', 'oRaNgE')
    False
    """
    list1 = list(s1)
    list2 = list(s2)
    list1.sort()
    list2.sort()
    return list1 == list2

def sont_anagrammes(s1:str, s2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ sont_anagrammes('orangé', 'orange')
    False
    """
    
def sont_anagrammes(s1:str, s2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    list1 = list(s1)
    list2 = list(s2)
    list1.sort()
    list2.sort()
    return list1 == list2

def occurrences(chaine:str):
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    occurrence = {}
    for caracteres in chaine:
        occurrence[caracteres] = occurrence.get(caracteres, 0) + 1
    return occurence

def sont_anagrammes(s1:str,s2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    occurrence1=occurrences(s1)
    occurrence2=occurrences(s2)
    return occurrence1 == occurrence2

def sont_anagrammes(s1:str, s2:str)->bool:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$ 

    """
    for caracteres in s1:
        if s1.count(caracteres) != s2.count(caracteres):
          return s1.count(caracteres) != s2.count(caracteres)
    return len(s1) == len(s2)

EQUIV_NON_ACCENTUE = {'à':'a', 'â':'a','ä':'a','é':'e','è':'e','ë':'e','î':'i','ï':'i','ö':'o','ô':'o','ù':'u','û':'u','ü':'u','ÿ':'y','ç':'c'}

def bas_casse_sans_accent(chaine:str)->str:
    """à_remplacer_par_ce_que_fait_la_fonction

    Précondition : 
    Exemple(s) :
    $$$  

    """
    EQUIV_NON_ACCENTUE = {'à':'a', 'â':'a','ä':'a','é':'e','è':'e','ë':'e','î':'i','ï':'i','ö':'o','ô':'o','ù':'u','û':'u','ü':'u','ÿ':'y','ç':'c'}
    chaine_tranformee = ''.join(EQUIV_NON_ACCENTUE.get(c.lower(),c.lower())for c in chaine)
    return chaine_transformee 
    
gg
    

    
        
        











